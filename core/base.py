import pygame, sys, datetime
from core.input import Input

class Base(object):
    
    def __init__(self, screenSize=[512,512]):

        pygame.init()
        # indicate rendering details
        displayFlags = pygame.DOUBLEBUF | pygame.OPENGL

        # initialise buffers for antialiasing
        pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLEBUFFERS, 1)
        pygame.display.gl_set_attribute(pygame.GL_MULTISAMPLESAMPLES, 4)
        
        # use core OpenGL profile for cross-platform compatibility
        pygame.display.gl_set_attribute(
            pygame.GL_CONTEXT_PROFILE_MASK,
            pygame.GL_CONTEXT_PROFILE_CORE
        )

        # create and display the window
        self.screen = pygame.display.set_mode(screenSize, displayFlags)

        # set the text in the title bar
        pygame.display.set_caption("Graphics Window")

        # determine active main loop
        self.running = True

        # manage time-related data and operations
        self.clock = pygame.time.Clock()

        # manage user input
        self.input = Input()

    def initialise(self):
        pass

    def update(self):
        pass

    def run(self):

        self.initialise()

        while self.running:

            ## process input ##
            if self.input.quit:
                self.running = False

            ## update ##
            self.update()

            ## render ##
            # display image on screen
            pygame.display.flip()

            # set to 60 fps
            self.clock.tick(60)
        
        ## shutdown ##
        pygame.exit()
        sys.exit()